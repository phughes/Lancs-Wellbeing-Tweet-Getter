// external modules
const dotenv = require('dotenv');
const fs = require('fs-extra');
const mongoose = require('mongoose');

// local modules
const getData = require('./api/getData');

// models
const Tweet = require('./api/models/tweetModel');

/* ################## *
 * CONFIG             *
 * ################## */
dotenv.config(); // load dotenv variables
const isProduction = process.env.NODE_ENV === 'production';

/* ################## *
 * MONGODB            *
 * ################## */
const writeToDB = (data) => {
  return new Promise((res, rej) => {
    // connect to Mongodb
    mongoose.connect(`mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PWD}@${process.env.MONGO_ARGS}`, (err) => {
      if (err) return rej(err);
      // config connection
      mongoose.promise = global.Promise;
      if (!isProduction) mongoose.set('debug', true);
      // loop through tweets per area
      // @todo an async function would be better here
      for (let area in data) {
        if (!data.hasOwnProperty(area)) continue;
        let tweets = data[area];
        for (let tweet in tweets) {
          if (!tweets.hasOwnProperty(tweet)) continue;
          let d = data[area][tweet]; // cache tweet
          let t = new Tweet({        // use Tweet schema
            id: tweet,
            area: area,
            time: Number(d[0]),
            age: d[1],
            gender: d[2],
            optimism: d[3],
            affect: d[4],
            intensity: d[5],
            POS_P: d[6],
            POS_E: d[7],
            POS_R: d[8],
            POS_M: d[9],
            POS_A: d[10],
            NEG_P: d[11],
            NEG_E: d[12],
            NEG_R: d[13],
            NEG_M: d[14],
            NEG_A: d[15],
          });
          t.save((err) => {
            if (err) {
              // if duplicate index, skip, else reject
              if (err.code && err.code === 11000) {
                t.next();
              } else {
                rej(err);
              }
            }
          });
        }
      }
    });
  });
};

/* ################## *
 * CSV                *
 * ################## */
const writeToCSV = (data) => {
  // CSV header row
  let csv = 'id,area,time,age,gender,optimism,affect,intensity,POS_P,POS_E,' +
      'POS_R,POS_M,POS_A,NEG_P,NEG_E,NEG_R,NEG_M,NEG_A\n';
  // loop through tweets per area
  // @todo an async function would be better here
  for (let area in data) {
    if (!data.hasOwnProperty(area)) continue;
    let tweets = data[area];
    for (let tweet in tweets) {
      if (!tweets.hasOwnProperty(tweet)) continue;
      let d = data[area][tweet]; // cache tweet
      // append each piece of twitter data to a new line in the CSV variable
      csv += `${tweet},${area}`;
      for (let i of d) {
        // ensure each item is a number
        csv += ',' + Number(i);
      }
      csv += '\n';
    }
  }
  fs.writeFile('lancs_tweets.txt', csv, (err) => console.error);
};

/* ################## *
 * TWITTER DATA       *
 * ################## */
const startDataCollection = () => {
  // main function
  const dataCollection = () => {
    getData()
    .then((data) => {
      writeToCSV(data);
      /* writeToDB(data)
      .then(() => {
        mongoose.disconnect();
        if (!isProduction) console.log('DONE!');
      })
      .catch((err) => {
        if (err && !isProduction) console.error(err);
        clearInterval(interval);
      }); */
    })
    .catch((err) => {
      if (err && !isProduction) console.error(err);
      clearInterval(interval);
    });
  };

  // run dataCollection the first time immediately
  dataCollection();

  // run dataCollection every 6 hours
  let interval = setInterval(dataCollection, 3600000);
};

startDataCollection();
