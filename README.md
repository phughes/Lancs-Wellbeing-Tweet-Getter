# Lancashire Wellbeing Tweet Getter
(catchier name TBD!)

Uses the Twitter node API to get Tweets from the 14 areas of Lancashire, mine them for wellbeing data, and then store them in a mongodb database or CSV file.
