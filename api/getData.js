(function() {
    'use strict';
    const isProduction = process.env.NODE_ENV === 'production';
    const getTweets = require('./getTweets.js');

    // Area names with Twitter geotags
    const areas = {
        'West Lancashire': '53.572675,-2.885481,5mi', // (Ormskirk)
        'Chorley': '53.653,-2.632,5mi',
        'South Ribble': '53.692,-2.697,5mi', // (Leyland)
        'Fylde': '53.7902,-2.8857,5mi', // (Wesham)
        'Preston': '53.759,-2.699,5mi',
        'Wyre': '53.903,-2.767,5mi', // (Garstang)
        'Lancaster': '54.047,-2.801,5mi',
        'Ribble Valley': '53.8711,-2.3916,5mi', // (Clitheroe)
        'Pendle': '53.8554,-2.1756,5mi', // (Colne)
        'Burnley': '53.789,-2.248,5mi',
        'Rossendale': '53.699,-2.291,5mi', // (Rawtenstall)
        'Hyndburn': '53.779200,-2.388444,5mi', // (Clayton-le-Moors)
        'Blackpool': '53.8175,-3.0357,5mi',
        'Blackburn with Darwen': '53.7449,-2.4769,5mi', // (Blackburn)
    };

    /**
     * @function getData
     * @return {Promise}
     */
    function getData() {
        return new Promise((res, rej) => {
            getTweets(areas).then((tweets) => {
                res(tweets);
            }).catch((err) => {
                rej(err);
            });
        });
    }
    module.exports = getData;
})();
