(function() {
    'use strict';
    const async = require('async');
    const anglo = require('british_american_translate');
    const moment = require('moment');
    const Twitter = require('twitter');
    const dotenv = require('dotenv').config();

    // wellbeing stats modules
    const predictAge = require('predictage');
    const predictGender = require('predictgender');
    const optimismo = require('optimismo');
    const affectimo = require('affectimo');
    const wellbeingAnalysis = require('wellbeing_analysis');

    // config
    const isProduction = process.env.NODE_ENV === 'production';
    const resultsTarget = 1000; // Target number of Tweets to recieve per area
    const maxLoops = 10; // Maximum number of loops to try and achieve resultsTarget
    const conLoops = 4; // Number of loops to run concurrently
    const opts = {'output': 'lex', 'places': 5, 'logs': 0}; // wellbeing stat module options
    // if (!isProduction) opts.logs = 3;

    // Twitter tokens
    const T = new Twitter({
        consumer_key: process.env.consumer_key,
        consumer_secret: process.env.consumer_secret,
        access_token_key: process.env.access_token_key,
        access_token_secret: process.env.access_token_secret,
    });

    // Twitter query info
    let lastTweetID = null;
    let rateLimitRemaining = 0;
    let rateLimitReset = 0;
    let rateLimitTotal = 0;
    let idList = {};

    /**
     * Clean up tweets for analysis
     * @function cleanText
     * @param  {string} str tweet text
     * @return {string}
     */
    function cleanText(str) {
        // emoji regex from https://mathiasbynens.be/notes/es-unicode-property-escapes#emoji
        const emojis = /\u{1F3F4}(?:\u{E0067}\u{E0062}(?:\u{E0065}\u{E006E}\u{E0067}|\u{E0077}\u{E006C}\u{E0073}|\u{E0073}\u{E0063}\u{E0074})\u{E007F}|\u200D\u2620\uFE0F)|\u{1F469}\u200D\u{1F469}\u200D(?:\u{1F466}\u200D\u{1F466}|\u{1F467}\u200D[\u{1F466}\u{1F467}])|\u{1F468}(?:\u200D(?:\u2764\uFE0F\u200D(?:\u{1F48B}\u200D)?\u{1F468}|[\u{1F468}\u{1F469}]\u200D(?:\u{1F466}\u200D\u{1F466}|\u{1F467}\u200D[\u{1F466}\u{1F467}])|\u{1F466}\u200D\u{1F466}|\u{1F467}\u200D[\u{1F466}\u{1F467}]|[\u{1F33E}\u{1F373}\u{1F393}\u{1F3A4}\u{1F3A8}\u{1F3EB}\u{1F3ED}\u{1F4BB}\u{1F4BC}\u{1F527}\u{1F52C}\u{1F680}\u{1F692}\u{1F9B0}-\u{1F9B3}])|[\u{1F3FB}-\u{1F3FF}]\u200D[\u{1F33E}\u{1F373}\u{1F393}\u{1F3A4}\u{1F3A8}\u{1F3EB}\u{1F3ED}\u{1F4BB}\u{1F4BC}\u{1F527}\u{1F52C}\u{1F680}\u{1F692}\u{1F9B0}-\u{1F9B3}])|\u{1F469}\u200D(?:\u2764\uFE0F\u200D(?:\u{1F48B}\u200D[\u{1F468}\u{1F469}]|[\u{1F468}\u{1F469}])|[\u{1F33E}\u{1F373}\u{1F393}\u{1F3A4}\u{1F3A8}\u{1F3EB}\u{1F3ED}\u{1F4BB}\u{1F4BC}\u{1F527}\u{1F52C}\u{1F680}\u{1F692}\u{1F9B0}-\u{1F9B3}])|\u{1F469}\u200D\u{1F466}\u200D\u{1F466}|(?:\u{1F441}\uFE0F\u200D\u{1F5E8}|\u{1F469}[\u{1F3FB}-\u{1F3FF}]\u200D[\u2695\u2696\u2708]|\u{1F468}(?:[\u{1F3FB}-\u{1F3FF}]\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708])|(?:[\u26F9\u{1F3CB}\u{1F3CC}\u{1F575}]\uFE0F|[\u{1F46F}\u{1F93C}\u{1F9DE}\u{1F9DF}])\u200D[\u2640\u2642]|[\u26F9\u{1F3CB}\u{1F3CC}\u{1F575}][\u{1F3FB}-\u{1F3FF}]\u200D[\u2640\u2642]|[\u{1F3C3}\u{1F3C4}\u{1F3CA}\u{1F46E}\u{1F471}\u{1F473}\u{1F477}\u{1F481}\u{1F482}\u{1F486}\u{1F487}\u{1F645}-\u{1F647}\u{1F64B}\u{1F64D}\u{1F64E}\u{1F6A3}\u{1F6B4}-\u{1F6B6}\u{1F926}\u{1F937}-\u{1F939}\u{1F93D}\u{1F93E}\u{1F9B8}\u{1F9B9}\u{1F9D6}-\u{1F9DD}](?:[\u{1F3FB}-\u{1F3FF}]\u200D[\u2640\u2642]|\u200D[\u2640\u2642])|\u{1F469}\u200D[\u2695\u2696\u2708])\uFE0F|\u{1F469}\u200D\u{1F467}\u200D[\u{1F466}\u{1F467}]|\u{1F469}\u200D\u{1F469}\u200D[\u{1F466}\u{1F467}]|\u{1F468}(?:\u200D(?:[\u{1F468}\u{1F469}]\u200D[\u{1F466}\u{1F467}]|[\u{1F466}\u{1F467}])|[\u{1F3FB}-\u{1F3FF}])|\u{1F3F3}\uFE0F\u200D\u{1F308}|\u{1F469}\u200D\u{1F467}|\u{1F469}[\u{1F3FB}-\u{1F3FF}]\u200D[\u{1F33E}\u{1F373}\u{1F393}\u{1F3A4}\u{1F3A8}\u{1F3EB}\u{1F3ED}\u{1F4BB}\u{1F4BC}\u{1F527}\u{1F52C}\u{1F680}\u{1F692}\u{1F9B0}-\u{1F9B3}]|\u{1F469}\u200D\u{1F466}|\u{1F1F6}\u{1F1E6}|\u{1F1FD}\u{1F1F0}|\u{1F1F4}\u{1F1F2}|\u{1F469}[\u{1F3FB}-\u{1F3FF}]|\u{1F1ED}[\u{1F1F0}\u{1F1F2}\u{1F1F3}\u{1F1F7}\u{1F1F9}\u{1F1FA}]|\u{1F1EC}[\u{1F1E6}\u{1F1E7}\u{1F1E9}-\u{1F1EE}\u{1F1F1}-\u{1F1F3}\u{1F1F5}-\u{1F1FA}\u{1F1FC}\u{1F1FE}]|\u{1F1EA}[\u{1F1E6}\u{1F1E8}\u{1F1EA}\u{1F1EC}\u{1F1ED}\u{1F1F7}-\u{1F1FA}]|\u{1F1E8}[\u{1F1E6}\u{1F1E8}\u{1F1E9}\u{1F1EB}-\u{1F1EE}\u{1F1F0}-\u{1F1F5}\u{1F1F7}\u{1F1FA}-\u{1F1FF}]|\u{1F1F2}[\u{1F1E6}\u{1F1E8}-\u{1F1ED}\u{1F1F0}-\u{1F1FF}]|\u{1F1F3}[\u{1F1E6}\u{1F1E8}\u{1F1EA}-\u{1F1EC}\u{1F1EE}\u{1F1F1}\u{1F1F4}\u{1F1F5}\u{1F1F7}\u{1F1FA}\u{1F1FF}]|\u{1F1FC}[\u{1F1EB}\u{1F1F8}]|\u{1F1FA}[\u{1F1E6}\u{1F1EC}\u{1F1F2}\u{1F1F3}\u{1F1F8}\u{1F1FE}\u{1F1FF}]|\u{1F1F0}[\u{1F1EA}\u{1F1EC}-\u{1F1EE}\u{1F1F2}\u{1F1F3}\u{1F1F5}\u{1F1F7}\u{1F1FC}\u{1F1FE}\u{1F1FF}]|\u{1F1EF}[\u{1F1EA}\u{1F1F2}\u{1F1F4}\u{1F1F5}]|\u{1F1F8}[\u{1F1E6}-\u{1F1EA}\u{1F1EC}-\u{1F1F4}\u{1F1F7}-\u{1F1F9}\u{1F1FB}\u{1F1FD}-\u{1F1FF}]|\u{1F1EE}[\u{1F1E8}-\u{1F1EA}\u{1F1F1}-\u{1F1F4}\u{1F1F6}-\u{1F1F9}]|\u{1F1FF}[\u{1F1E6}\u{1F1F2}\u{1F1FC}]|\u{1F1EB}[\u{1F1EE}-\u{1F1F0}\u{1F1F2}\u{1F1F4}\u{1F1F7}]|\u{1F1F5}[\u{1F1E6}\u{1F1EA}-\u{1F1ED}\u{1F1F0}-\u{1F1F3}\u{1F1F7}-\u{1F1F9}\u{1F1FC}\u{1F1FE}]|\u{1F1E9}[\u{1F1EA}\u{1F1EC}\u{1F1EF}\u{1F1F0}\u{1F1F2}\u{1F1F4}\u{1F1FF}]|\u{1F1F9}[\u{1F1E6}\u{1F1E8}\u{1F1E9}\u{1F1EB}-\u{1F1ED}\u{1F1EF}-\u{1F1F4}\u{1F1F7}\u{1F1F9}\u{1F1FB}\u{1F1FC}\u{1F1FF}]|\u{1F1E7}[\u{1F1E6}\u{1F1E7}\u{1F1E9}-\u{1F1EF}\u{1F1F1}-\u{1F1F4}\u{1F1F6}-\u{1F1F9}\u{1F1FB}\u{1F1FC}\u{1F1FE}\u{1F1FF}]|[#\*0-9]\uFE0F\u20E3|\u{1F1F1}[\u{1F1E6}-\u{1F1E8}\u{1F1EE}\u{1F1F0}\u{1F1F7}-\u{1F1FB}\u{1F1FE}]|\u{1F1E6}[\u{1F1E8}-\u{1F1EC}\u{1F1EE}\u{1F1F1}\u{1F1F2}\u{1F1F4}\u{1F1F6}-\u{1F1FA}\u{1F1FC}\u{1F1FD}\u{1F1FF}]|\u{1F1F7}[\u{1F1EA}\u{1F1F4}\u{1F1F8}\u{1F1FA}\u{1F1FC}]|\u{1F1FB}[\u{1F1E6}\u{1F1E8}\u{1F1EA}\u{1F1EC}\u{1F1EE}\u{1F1F3}\u{1F1FA}]|\u{1F1FE}[\u{1F1EA}\u{1F1F9}]|[\u{1F3C3}\u{1F3C4}\u{1F3CA}\u{1F46E}\u{1F471}\u{1F473}\u{1F477}\u{1F481}\u{1F482}\u{1F486}\u{1F487}\u{1F645}-\u{1F647}\u{1F64B}\u{1F64D}\u{1F64E}\u{1F6A3}\u{1F6B4}-\u{1F6B6}\u{1F926}\u{1F937}-\u{1F939}\u{1F93D}\u{1F93E}\u{1F9B8}\u{1F9B9}\u{1F9D6}-\u{1F9DD}][\u{1F3FB}-\u{1F3FF}]|[\u26F9\u{1F3CB}\u{1F3CC}\u{1F575}][\u{1F3FB}-\u{1F3FF}]|[\u261D\u270A-\u270D\u{1F385}\u{1F3C2}\u{1F3C7}\u{1F442}\u{1F443}\u{1F446}-\u{1F450}\u{1F466}\u{1F467}\u{1F470}\u{1F472}\u{1F474}-\u{1F476}\u{1F478}\u{1F47C}\u{1F483}\u{1F485}\u{1F4AA}\u{1F574}\u{1F57A}\u{1F590}\u{1F595}\u{1F596}\u{1F64C}\u{1F64F}\u{1F6C0}\u{1F6CC}\u{1F918}-\u{1F91C}\u{1F91E}\u{1F91F}\u{1F930}-\u{1F936}\u{1F9B5}\u{1F9B6}\u{1F9D1}-\u{1F9D5}][\u{1F3FB}-\u{1F3FF}]|[\u261D\u26F9\u270A-\u270D\u{1F385}\u{1F3C2}-\u{1F3C4}\u{1F3C7}\u{1F3CA}-\u{1F3CC}\u{1F442}\u{1F443}\u{1F446}-\u{1F450}\u{1F466}-\u{1F469}\u{1F46E}\u{1F470}-\u{1F478}\u{1F47C}\u{1F481}-\u{1F483}\u{1F485}-\u{1F487}\u{1F4AA}\u{1F574}\u{1F575}\u{1F57A}\u{1F590}\u{1F595}\u{1F596}\u{1F645}-\u{1F647}\u{1F64B}-\u{1F64F}\u{1F6A3}\u{1F6B4}-\u{1F6B6}\u{1F6C0}\u{1F6CC}\u{1F918}-\u{1F91C}\u{1F91E}\u{1F91F}\u{1F926}\u{1F930}-\u{1F939}\u{1F93D}\u{1F93E}\u{1F9B5}\u{1F9B6}\u{1F9B8}\u{1F9B9}\u{1F9D1}-\u{1F9DD}][\u{1F3FB}-\u{1F3FF}]?|[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u270A\u270B\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55\u{1F004}\u{1F0CF}\u{1F18E}\u{1F191}-\u{1F19A}\u{1F1E6}-\u{1F1FF}\u{1F201}\u{1F21A}\u{1F22F}\u{1F232}-\u{1F236}\u{1F238}-\u{1F23A}\u{1F250}\u{1F251}\u{1F300}-\u{1F320}\u{1F32D}-\u{1F335}\u{1F337}-\u{1F37C}\u{1F37E}-\u{1F393}\u{1F3A0}-\u{1F3CA}\u{1F3CF}-\u{1F3D3}\u{1F3E0}-\u{1F3F0}\u{1F3F4}\u{1F3F8}-\u{1F43E}\u{1F440}\u{1F442}-\u{1F4FC}\u{1F4FF}-\u{1F53D}\u{1F54B}-\u{1F54E}\u{1F550}-\u{1F567}\u{1F57A}\u{1F595}\u{1F596}\u{1F5A4}\u{1F5FB}-\u{1F64F}\u{1F680}-\u{1F6C5}\u{1F6CC}\u{1F6D0}-\u{1F6D2}\u{1F6EB}\u{1F6EC}\u{1F6F4}-\u{1F6F9}\u{1F910}-\u{1F93A}\u{1F93C}-\u{1F93E}\u{1F940}-\u{1F945}\u{1F947}-\u{1F970}\u{1F973}-\u{1F976}\u{1F97A}\u{1F97C}-\u{1F9A2}\u{1F9B0}-\u{1F9B9}\u{1F9C0}-\u{1F9C2}\u{1F9D0}-\u{1F9FF}]|[#\*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u265F\u2660\u2663\u2665\u2666\u2668\u267B\u267E\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299\u{1F004}\u{1F0CF}\u{1F170}\u{1F171}\u{1F17E}\u{1F17F}\u{1F18E}\u{1F191}-\u{1F19A}\u{1F1E6}-\u{1F1FF}\u{1F201}\u{1F202}\u{1F21A}\u{1F22F}\u{1F232}-\u{1F23A}\u{1F250}\u{1F251}\u{1F300}-\u{1F321}\u{1F324}-\u{1F393}\u{1F396}\u{1F397}\u{1F399}-\u{1F39B}\u{1F39E}-\u{1F3F0}\u{1F3F3}-\u{1F3F5}\u{1F3F7}-\u{1F4FD}\u{1F4FF}-\u{1F53D}\u{1F549}-\u{1F54E}\u{1F550}-\u{1F567}\u{1F56F}\u{1F570}\u{1F573}-\u{1F57A}\u{1F587}\u{1F58A}-\u{1F58D}\u{1F590}\u{1F595}\u{1F596}\u{1F5A4}\u{1F5A5}\u{1F5A8}\u{1F5B1}\u{1F5B2}\u{1F5BC}\u{1F5C2}-\u{1F5C4}\u{1F5D1}-\u{1F5D3}\u{1F5DC}-\u{1F5DE}\u{1F5E1}\u{1F5E3}\u{1F5E8}\u{1F5EF}\u{1F5F3}\u{1F5FA}-\u{1F64F}\u{1F680}-\u{1F6C5}\u{1F6CB}-\u{1F6D2}\u{1F6E0}-\u{1F6E5}\u{1F6E9}\u{1F6EB}\u{1F6EC}\u{1F6F0}\u{1F6F3}-\u{1F6F9}\u{1F910}-\u{1F93A}\u{1F93C}-\u{1F93E}\u{1F940}-\u{1F945}\u{1F947}-\u{1F970}\u{1F973}-\u{1F976}\u{1F97A}\u{1F97C}-\u{1F9A2}\u{1F9B0}-\u{1F9B9}\u{1F9C0}-\u{1F9C2}\u{1F9D0}-\u{1F9FF}]\uFE0F/gu;
        const handle = /@\w{1,15}/gmi; // @ twitter handle
        const links = /(?:https?):\/\/[\n\S]+/gmi; // URLs
        const nl = /\\n|\n/gm; // new lines
        const quotes = /\\"/gm; // quote marks
        const elips = /[.]{3}/gm; // elipses
        const spaces = /\s{2,}/gm; // multiple spaces
        let clean = str.replace('&amp;', 'and').replace(emojis, '').replace(handle, '').replace(links, '').replace(quotes, '').replace(/%26/g, '&').replace(/%23/g, '#').replace(/%3B/g, ';').replace(elips, '');
        clean = clean.replace('#', '');
        clean = clean.replace(nl, ' ').replace(spaces, ' ').trim();
        if (!clean) {
            return null;
        } else {
            // convert to US English because the WWBP lexica are American
            return anglo.uk2us(clean).toLowerCase();
        }
    }

    /**
     * @function twitterSearch
     * @param  {string} area    area name
     * @param  {string} geocode Twitter geocode
     * @return {Promise}
     */
    function twitterSearch(area, geocode) {
        // Twitter search query options
        const options = {
            q: '-RT AND -filter:retweets AND -filter:verified',
            geocode: geocode,
            result_type: 'recent',
            count: 100,
            lang: 'en',
            max_id: lastTweetID,
        };

        const output = {};
        if (!idList[area]) idList[area] = [];

        return new Promise((res, rej) => {
            T.get('search/tweets', options, function(err, data, response) {
                // Handle errors
                if (err) return rej(err);
                // Update rate limit info
                rateLimitRemaining = response.headers['x-rate-limit-remaining'];
                rateLimitReset = moment(response.headers['x-rate-limit-reset'], 'X').diff(moment(), 'milliseconds');
                rateLimitTotal = response.headers['x-rate-limit-limit'];
                // Loop through returned tweets
                const tweets = data.statuses;
                const ids = idList[area];
                async.eachOf(tweets, (t, key, callback) => {
                    // only process unique tweets
                    if (ids.indexOf(t.id_str) === -1) {
                        // clean text
                        let tw = t.text;
                        if (t.extended_tweet) tw = t.extended_tweet.full_text ? t.extended_tweet.full_text : t.text;
                        let clean = cleanText(tw);
                        if (clean) {
                            // clean timestamp
                            const time = Number(moment(t.created_at, 'dd MMM DD HH:mm:ss ZZ YYYY', 'en').format('x'));
                            // get stats
                            const age = predictAge(clean, opts).AGE;
                            const gender = predictGender(clean, opts).GENDER;
                            const optimism = optimismo(clean);
                            const ai = affectimo(clean, opts);
                            const perma = wellbeingAnalysis(clean, opts);
                            // push tweet text to output object
                            output[t.id_str] = [time, age, gender, optimism, ai.AFFECT, ai.INTENSITY, perma.POS_P, perma.NEG_P, perma.POS_E, perma.NEG_E, perma.POS_R, perma.NEG_R, perma.POS_M, perma.NEG_M, perma.POS_A, perma.NEG_A];
                            // make sure we don't process duplicate tweets
                            idList[area].push(t.id_str);
                        }
                    }
                    // On next loop, only return tweets older than this
                    lastTweetID = t.id;
                    options.max_id = lastTweetID;
                    callback();
                }, (err) => {
                    if (err) return rej(err);
                    res(output);
                });
            });
        });
    }

    /**
     * @function mainLoop
     * @param  {Object} areas location names: geocodes
     * @return {Promise}
     */
    function mainLoop(areas) {
        const output = {};

        return new Promise((res, rej) => {
            async.eachOfLimit(areas, conLoops, function(geocode, area, callback) {
                // Reset counters
                let loop = 0; // loops within area
                lastTweetID = null;

                /**
                 * @function twitterLoop
                 * @param  {string} area    area name
                 * @param  {string} geocode Twitter geocode
                 */
                function twitterLoop(area, geocode) {
                    // rate limit time to reset in mins
                    const resetTime = ((rateLimitReset / 1000) / 60).toFixed();
                    // create output array if this is our first loop
                    if (!output[area]) output[area] = {};
                    // perform twitter search
                    twitterSearch(area, geocode).then((tweets) => {
                        // concat output[area] object with new tweets
                        output[area] = {...output[area], ...tweets};
                        // n = current number of tweets found in area
                        const n = Object.keys(output[area]).length;
                        if (!isProduction) console.log(`>Loop ${loop} for ${area}. Currently ${n} tweets on record. Rate limit remaining: ${rateLimitRemaining}/${rateLimitTotal} (${resetTime} minutes to reset).`);
                        // if we're looping and getting nothing back
                        if (n === 0 && loop > 1) {
                            callback(`Not finding tweets for ${area}. Abandoning.`);
                        // if we have the required number of tweets,
                        // have reached maxLoops,
                        // or the last loop produced no new results....
                        } else if (n >= resultsTarget || loop >= maxLoops) {
                            if (!isProduction) console.log(`Finished ${area} on loop ${loop} with ${n} tweets.`);
                            // Reset counters
                            loop = 0;
                            // if we are far enough away from the rate limit, move on
                            // else set a timer equal to rateLimitReset
                            if (rateLimitRemaining >= maxLoops) {
                                if (!isProduction) console.log(`Moving on immediately. Rate limit remaining: ${rateLimitRemaining}/${rateLimitTotal}.`);
                                callback();
                            } else {
                                if (!isProduction) console.log(`Timed out for ${resetTime} minutes (${moment().add(resetTime, 'minutes').format('HH:mm:ss')}). Rate limit remaining: ${rateLimitRemaining}/${rateLimitTotal}.`);
                                setTimeout(function() {
                                    callback();
                                }, rateLimitReset);
                            }
                        // if we haven't hit any of the above limits, loop!
                        } else {
                            // update counters
                            loop += 1;
                            // loop again
                            twitterLoop(area, geocode);
                        }
                    }).catch((err) => {
                        rej(err);
                    });
                }
                // First loop
                twitterLoop(area, geocode);
            }, function(err) {
                if (err) return rej(err);
                res(output);
            });
        });
    }

    /**
     * @function getTweets
     * @param  {Object} obj {location name: geocode}
     * @return {Promise}
     */
    function getTweets(obj) {
        return new Promise((res, rej) => {
            mainLoop(obj).then((tweets) => {
                res(tweets);
            }).catch((err) =>{
                rej(err);
            });
        });
    }
    module.exports = getTweets;
})();
