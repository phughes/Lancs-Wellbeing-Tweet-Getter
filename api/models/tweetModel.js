const mongoose = require('mongoose');

const tweetSchema = mongoose.Schema({
    id: {
        type: String,
        required: true,
        unique: true,
    },
    area: String,
    time: Number,
    age: Number,
    gender: Number,
    optimism: Number,
    affect: Number,
    intensity: Number,
    POS_P: Number,
    POS_E: Number,
    POS_R: Number,
    POS_M: Number,
    POS_A: Number,
    NEG_P: Number,
    NEG_E: Number,
    NEG_R: Number,
    NEG_M: Number,
    NEG_A: Number,
}, {strict: true, emitIndexErrors: true});

const handleE11000 = (error, res, next) => {
    if (error.name === 'MongoError' && error.code === 11000) {
      next(new Error('There was a duplicate key error'));
    } else {
      next();
    }
};

tweetSchema.post('save', handleE11000);

module.exports = mongoose.model('Tweet', tweetSchema);
